function filterBy(arr, type) {
  return arr.filter(elem => typeof elem !== type)
}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));