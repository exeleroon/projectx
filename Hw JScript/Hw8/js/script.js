const inputEl = document.querySelector('#input');
const divEl = document.querySelector('.divIn')
inputEl.addEventListener('mouseenter', function () {
  inputEl.style.border = '3px solid green'
});

inputEl.addEventListener('blur', function () {
  inputEl.style.border = '1px solid black';
  inputEl.style.color = 'green';
  const inputText = inputEl.value;
  if (inputText <= 0) {
    inputEl.style.border = '3px solid red';
    const newP = document.createElement('p');
    newP.innerText = 'Please enter correct price!'
    return inputEl.after(newP);
  };
  const newSpan = document.createElement('span');
  const newBtn = document.createElement('button');
  newBtn.innerText = 'X';
  newSpan.innerText = ` Current Price: ${inputText} `;
  newSpan.append(newBtn);
  divEl.before(newSpan);
  newBtn.addEventListener('click', function () {
    newSpan.remove(newSpan);
  })
});




