let counter = 0;
let interval;
const allImg = document.querySelectorAll('.image-to-show');
const iW = document.querySelector('.images-wrapper');

function watch() { 
  const newDiv = document.createElement('div');
  const btnPause = document.createElement('button');
  const btnPlay = document.createElement('button');
  btnPause.innerText = 'PAUSE';
  btnPlay.innerText = 'CONTINUE';
  btnPause.style.width = '180px';
  btnPlay.style.width = '180px';
  btnPause.style.height = '50px';
  btnPlay.style.height = '50px';
  btnPause.style.margin = '10px';
  iW.after(newDiv);
  newDiv.append(btnPause);
  newDiv.append(btnPlay);

  btnPause.addEventListener('click', function () {
    clearInterval(interval);
  });
  btnPlay.addEventListener('click', function () {
    interval = setInterval(() => {
      allImg[counter].classList.remove('active');
      allImg[counter].style.display = 'none';
      counter++;
      if (counter === allImg.length) {
        counter = 0;
      } allImg[counter].classList.add('active');
        allImg[counter].style.display = 'flex';
    }, 1000);
})
}
watch()