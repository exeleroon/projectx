const btnTheme = document.querySelector('.btn-theme');

btnTheme.addEventListener('click', function () {
    document.body.classList.toggle('evil-theme');
    if (document.body.classList.contains('evil-theme')) {
        btnTheme.innerText = 'Return Stock Theme';
        localStorage.setItem('theme', 'evil-theme');
    } else {
        btnTheme.innerText = 'Change Theme';
        localStorage.removeItem('theme');
        }
    });
    window.onload = () => {
        if(localStorage.getItem('theme')) {
            document.body.classList.add('evil-theme');
            btnTheme.innerText = 'Return Stock Theme';
        }
    }
