const tabsEl = document.querySelectorAll('.tabs-title');
const contentEl = document.querySelectorAll('.textElem');

function showText(event) {
  for (let i = 0; i < tabsEl.length; i++) {
    if (tabsEl[i] === event.target) {
      event.target.classList.add('active');
      contentEl[i].classList.add('textElem-active');
      contentEl[i].style.display = 'block';
    } else {
      tabsEl[i].classList.remove('active');
      contentEl[i].classList.remove('textElem-active');
      contentEl[i].style.display = 'none';
    }
  }
}
document.querySelector('.tabs').addEventListener('click', showText)