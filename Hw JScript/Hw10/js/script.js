const eyes = document.querySelectorAll('.fas');
const inputs = document.querySelectorAll('input');


for (let i = 0; i < eyes.length; ++i) {
  eyes[i].addEventListener('click', function (event) {
    if (event.target.classList.contains('fa-eye')) {
      event.target.classList.toggle('fa-eye-slash');
      event.target.classList.toggle('fa-eye');
      
      inputs[i].type = 'password';
    } else {
      event.target.classList.toggle('fa-eye');
      event.target.classList.toggle('fa-eye-slash');
      inputs[i].type = 'text';
    }
  })
}

const fInput = document.querySelector('.first-input');
const sInput = document.querySelector('.second-input');
const logBtn = document.querySelector('.btn');

logBtn.addEventListener('click', function name() {
  if (fInput.value === sInput.value && fInput.value !== '') {
     alert('Welcome')
  } else if (fInput.value !== sInput.value || fInput.value === '' || sInput.value === '') {
    const inpWr = document.querySelectorAll('.input-wrapper')[1];
    const textP = document.createElement('p');
    inpWr.append(textP);
    textP.innerText = 'incorrect password, try again';
    textP.style.color = 'red';
    logBtn.disabled = true;
    setTimeout(() => {
      logBtn.disabled = false;
      textP.remove();
    }, 3000);
    return `${fInput.value = ''}  ${sInput.value = ''}`
  }})

